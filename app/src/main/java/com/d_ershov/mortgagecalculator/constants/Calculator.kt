package com.d_ershov.mortgagecalculator.constants

object Calculator {
    val MAX_SUM_CREDIT = 10000000
    val MAX_PERIOD_MONTH = 60
    val MAX_PERCENT = 146
}