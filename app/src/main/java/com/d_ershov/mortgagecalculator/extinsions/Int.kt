package com.d_ershov.mortgagecalculator.extinsions

import com.d_ershov.mortgagecalculator.dto.PeriodDto
import java.text.NumberFormat
import kotlin.math.floor

fun Int.roundUp() : Int {
    var mNumber: Int
    if (this < 1000000) {
        mNumber = this / 10000
        mNumber *= 10000
    } else {
        mNumber = this / 100000
        mNumber *= 100000
    }
    return mNumber
}

fun Int.toFormat() : String {
    return NumberFormat.getInstance().format(this)
}

fun Int.splitMonthsByPeriod() : PeriodDto {
    val dto = PeriodDto()
    if (this < 12) {
        dto.months = this
    } else {
        dto.months = this % 12
        dto.years = floor((this / 12).toDouble()).toInt()
    }
    return dto
}

fun Int.declineWord(words: Array<String>) : String {
    var result = ""
    if (this % 100 in 10..20) {
        result = words[2]
    } else if(this % 10 == 0 || this % 10 > 4) {
        result = words[2]
    } else if (this % 10 > 1) {
        result = words[1]
    } else {
        result = words[0]
    }
    return "$this $result"
}
