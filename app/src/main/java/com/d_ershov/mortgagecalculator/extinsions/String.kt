package com.d_ershov.mortgagecalculator.extinsions

import java.text.NumberFormat

fun String.toFormat() : String {
    return if (this.isEmpty()) {
        ""
    } else {
        return NumberFormat.getInstance().format(this.replaceSpaces().toInt())
    }
}

fun String.replaceSpaces() : String {
    return if (this.isEmpty()) {
        ""
    } else {
        return this.replace("\\s".toRegex(), "")
    }
}

fun String.countChars(char: String) : Int {
    return this.length - this.replace(char, "").length
}

fun String.safeToInt() : Int {
    if (this.isEmpty()) {
        return 0
    }
    return this.replaceSpaces().toInt()
}

fun String.safeToDouble() : Double {
    if (this.isEmpty()) {
        return 0.0
    }
    return this.replaceSpaces().toDouble()
}