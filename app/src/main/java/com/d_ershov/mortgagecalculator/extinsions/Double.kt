package com.d_ershov.mortgagecalculator.extinsions

import java.text.NumberFormat

fun Double.toFormat() : String {
    return NumberFormat.getCurrencyInstance().format(this)
}
