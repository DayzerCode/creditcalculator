package com.d_ershov.mortgagecalculator.ui.listeners.validators

import com.d_ershov.mortgagecalculator.R
import com.d_ershov.mortgagecalculator.constants.Calculator

class CalculatorValidator(private val sum : Int, private val percent : Double, private val countMouths: Int) : BaseMultipleValidator() {
    fun isValid() : Boolean {
        if (sum <= 0) {
            errors.add(resources.getString(R.string.fragment_home_error_sum_required))
        } else if(sum > Calculator.MAX_SUM_CREDIT) {
            errors.add(resources.getString(R.string.fragment_home_error_exceeded_max_sum_credit))
        }

        if (percent <= 0) {
            errors.add(resources.getString(R.string.fragment_home_error_percent_required))
        } else if(percent > Calculator.MAX_PERCENT) {
            errors.add(resources.getString(R.string.fragment_home_error_exceeded_max_percent))
        }

        if (countMouths <= 0) {
            errors.add(resources.getString(R.string.fragment_home_error_period_required))
        } else if(countMouths > Calculator.MAX_PERIOD_MONTH) {
            errors.add(resources.getString(R.string.fragment_home_error_exceeded_max_period))
        }

        return errors.size == 0
    }
}