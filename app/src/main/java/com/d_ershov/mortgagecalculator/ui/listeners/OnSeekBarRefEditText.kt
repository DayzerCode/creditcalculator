package com.d_ershov.mortgagecalculator.ui.listeners

import android.widget.EditText
import android.widget.SeekBar
import com.d_ershov.mortgagecalculator.extinsions.roundUp
import com.d_ershov.mortgagecalculator.extinsions.toFormat

class OnSeekBarRefEditText(private val et : EditText) : SeekBar.OnSeekBarChangeListener {
    override fun onProgressChanged(
        seekBar: SeekBar,
        progress: Int,
        fromUser: Boolean
    ) {
        if (fromUser) {
            et.setText(progress.roundUp().toFormat())
        }
    }
    override fun onStartTrackingTouch(seekBar: SeekBar) {}
    override fun onStopTrackingTouch(seekBar: SeekBar) {}
}