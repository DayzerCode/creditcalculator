package com.d_ershov.mortgagecalculator.ui.listeners

import android.annotation.SuppressLint
import android.widget.EditText
import android.widget.SeekBar
import com.d_ershov.mortgagecalculator.App
import com.d_ershov.mortgagecalculator.R
import com.d_ershov.mortgagecalculator.extinsions.declineWord
import com.d_ershov.mortgagecalculator.extinsions.splitMonthsByPeriod

class OnSeekBarRefEditTextPeriod(private val et : EditText) : SeekBar.OnSeekBarChangeListener {
    private val resources = App.resourcesApp
    @SuppressLint("SetTextI18n")
    override fun onProgressChanged(
        seekBar: SeekBar,
        progress: Int,
        fromUser: Boolean
    ) {
        if (fromUser) {
            var resultString = ""
            val period = progress.splitMonthsByPeriod()
            if (period.years > 0) {
                resultString += period.years.declineWord(getArrayYears()) + " "
            }
            if (period.months > 0) {
                resultString +=  period.months.declineWord(getArrayMonths()) + " "
            }
            et.setText(resultString)
        }
    }
    override fun onStartTrackingTouch(seekBar: SeekBar) {}
    override fun onStopTrackingTouch(seekBar: SeekBar) {}

    private fun getArrayMonths() : Array<String> {
        return arrayOf(
            resources.getString(R.string.decline_month_one),
            resources.getString(R.string.decline_month_up_five),
            resources.getString(R.string.decline_month_after_five)
        )
    }

    private fun getArrayYears() : Array<String> {
        return arrayOf(
            resources.getString(R.string.decline_year_one),
            resources.getString(R.string.decline_year_up_five),
            resources.getString(R.string.decline_year_after_five)
        )
    }
}