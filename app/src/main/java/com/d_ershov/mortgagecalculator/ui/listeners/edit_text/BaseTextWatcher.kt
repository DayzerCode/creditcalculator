package com.d_ershov.mortgagecalculator.ui.listeners.edit_text

import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import com.d_ershov.mortgagecalculator.ui.listeners.validators.BaseSingleValidator

abstract class BaseTextWatcher() : TextWatcher {
    val validators =  ArrayList<BaseSingleValidator>()
    protected open lateinit var et: EditText
    protected var sourceValue = ""
    private var lastError = ""

    abstract fun textChanged(s: CharSequence, start: Int, before: Int, count: Int)

    override fun onTextChanged(
        s: CharSequence, start: Int,
        before: Int, count: Int
    ) {
        if (s.isEmpty() || sourceValue == et.text.toString()) {
            return
        }
        et.removeTextChangedListener(this)
        textChanged(s, start, before, count)
        et.addTextChangedListener(this)
    }

    override fun beforeTextChanged(
        s: CharSequence, start: Int,
        count: Int, after: Int
    ) {
        sourceValue = s.toString()
    }

    override fun afterTextChanged(s: Editable) {}

    protected fun showError() {
        et.error = lastError
        et.setText(sourceValue)
        et.setSelection(et.text.length)
        Handler().postDelayed({
            et.error = null
        }, 2000)
    }

    protected fun isValid(string: String) : Boolean {
        if (validators.size > 0) {
            for (validator in validators) {
                if (!validator.isValid(string)) {
                    lastError = validator.errorMsg
                    return false
                }
            }
        }
        return true
    }
}