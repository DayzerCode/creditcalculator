package com.d_ershov.mortgagecalculator.ui.listeners.validators

import com.d_ershov.mortgagecalculator.R
import com.d_ershov.mortgagecalculator.constants.Calculator

class ValidatorPercent() : BaseSingleValidator() {
    override fun isValid(string: String) : Boolean {
        return if (string.toDouble() <= Calculator.MAX_PERCENT.toDouble()) {
            true
        } else {
            errorMsg = resources.getString(R.string.fragment_home_error_exceeded_max_percent) + Calculator.MAX_PERCENT.toString()
            false
        }
    }
}