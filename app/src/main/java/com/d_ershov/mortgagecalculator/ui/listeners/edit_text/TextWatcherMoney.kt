package com.d_ershov.mortgagecalculator.ui.listeners.edit_text

import android.widget.EditText
import android.widget.SeekBar
import com.d_ershov.mortgagecalculator.extinsions.replaceSpaces
import com.d_ershov.mortgagecalculator.extinsions.toFormat

class TextWatcherMoney(override var et: EditText, private val sb : SeekBar? = null) : BaseTextWatcher() {
    override fun textChanged(s: CharSequence, start: Int, before: Int, count: Int) {
        if (isValid(s.toString())) {
            formatText(s.toString())
        } else {
            showError()
        }
    }

    private fun formatText(text: String) {
        val formatString = text.toFormat()
        et.setText(formatString)
        et.setSelection(formatString.length)
        sb?.let {
            sb.progress = text.replaceSpaces().toInt()
        }
    }
}