package com.d_ershov.mortgagecalculator.ui.listeners.validators

import com.d_ershov.mortgagecalculator.App

abstract class BaseMultipleValidator() {
    var errors = mutableListOf<String>()
    protected val resources = App.resourcesApp
}