package com.d_ershov.mortgagecalculator.ui.home

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.d_ershov.mortgagecalculator.R
import com.d_ershov.mortgagecalculator.calculators.AnnuityCreditCalculator
import com.d_ershov.mortgagecalculator.constants.Calculator
import com.d_ershov.mortgagecalculator.extinsions.safeToDouble
import com.d_ershov.mortgagecalculator.extinsions.safeToInt
import com.d_ershov.mortgagecalculator.extinsions.toFormat
import com.d_ershov.mortgagecalculator.ui.listeners.OnSeekBarRefEditText
import com.d_ershov.mortgagecalculator.ui.listeners.OnSeekBarRefEditTextPeriod
import com.d_ershov.mortgagecalculator.ui.listeners.edit_text.TextWatcherDouble
import com.d_ershov.mortgagecalculator.ui.listeners.edit_text.TextWatcherMoney
import com.d_ershov.mortgagecalculator.ui.listeners.validators.CalculatorValidator
import com.d_ershov.mortgagecalculator.ui.listeners.validators.ValidatorDouble
import com.d_ershov.mortgagecalculator.ui.listeners.validators.ValidatorMoney
import com.d_ershov.mortgagecalculator.ui.listeners.validators.ValidatorPercent
import kotlinx.android.synthetic.main.fragment_home.*


class HomeFragment : Fragment() {
    private lateinit var homeViewModel: HomeViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_home, container, false)
        return root
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // init
        sbCreditSum.max = Calculator.MAX_SUM_CREDIT
        sbPeriodCredit.max = Calculator.MAX_PERIOD_MONTH

        sbCreditSum.setOnSeekBarChangeListener(OnSeekBarRefEditText(etCreditSum))
        sbPeriodCredit.setOnSeekBarChangeListener(OnSeekBarRefEditTextPeriod(etPeriodCredit))

        val textWatcherMoney = TextWatcherMoney(etCreditSum, sbCreditSum)
        textWatcherMoney.validators.add(ValidatorMoney())
        etCreditSum.addTextChangedListener(textWatcherMoney)

        val textWatcherDouble = TextWatcherDouble(etPercentCredit)
        textWatcherDouble.validators.addAll(arrayListOf(ValidatorDouble(), ValidatorPercent()))
        etPercentCredit.addTextChangedListener(textWatcherDouble)

        btnCalculate.setOnClickListener {
            val sum = etCreditSum.text.toString().safeToInt()
            val percent = etPercentCredit.text.toString().safeToDouble()
            val countMouths = sbPeriodCredit.progress

            val validator = CalculatorValidator(sum, percent, countMouths)
            if (validator.isValid()) {
                val resultDto = AnnuityCreditCalculator(sum, percent, countMouths).getCreditResult()

                tvMonthlyPaymentValue.text = resultDto.monthlyPayment.toFormat()
                tvOverPaymentValue.text = resultDto.overPayment.toFormat()
                tvTotalSumValue.text = resultDto.totalSum.toFormat()
            } else {
                Toast.makeText(context, R.string.fragment_home_fields_required, Toast.LENGTH_SHORT)
                    .show()
            }
        }
    }
}
