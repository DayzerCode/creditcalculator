package com.d_ershov.mortgagecalculator.ui.listeners.validators

import com.d_ershov.mortgagecalculator.R
import com.d_ershov.mortgagecalculator.extinsions.countChars

class ValidatorDouble() : BaseSingleValidator() {
    override fun isValid(string: String) : Boolean {
        if (string[0].equals(',') || string[0].equals('.')) {
            errorMsg = resources.getString(R.string.fragment_home_error_first_symbol)
            return false
        }
        if ((string.countChars(",") > 1 || string.countChars(".") > 1) || (string.countChars(",") == 1 && string.countChars(".") == 1)) {
            errorMsg =  resources.getString(R.string.fragment_home_error_seporator)
            return false
        }
        return true
    }
}