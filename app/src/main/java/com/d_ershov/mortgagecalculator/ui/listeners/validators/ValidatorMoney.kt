package com.d_ershov.mortgagecalculator.ui.listeners.validators

import com.d_ershov.mortgagecalculator.R
import com.d_ershov.mortgagecalculator.constants.Calculator
import com.d_ershov.mortgagecalculator.extinsions.replaceSpaces

class ValidatorMoney() : BaseSingleValidator() {
    override fun isValid(string: String) : Boolean {
        val value = string.replaceSpaces().toInt()
        return if (value <= Calculator.MAX_SUM_CREDIT) {
            true
        } else {
            errorMsg = resources.getString(R.string.fragment_home_error_exceeded_max_sum_credit)
            false
        }
    }
}