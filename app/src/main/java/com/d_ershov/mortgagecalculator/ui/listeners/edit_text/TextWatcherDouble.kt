package com.d_ershov.mortgagecalculator.ui.listeners.edit_text

import android.widget.EditText

class TextWatcherDouble(override var et: EditText) :
    BaseTextWatcher() {
    override fun textChanged(s: CharSequence, start: Int, before: Int, count: Int) {
        if (!isValid(et.text.toString())) {
            showError()
        }
    }
}