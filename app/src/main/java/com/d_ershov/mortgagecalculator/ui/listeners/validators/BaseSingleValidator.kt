package com.d_ershov.mortgagecalculator.ui.listeners.validators

import com.d_ershov.mortgagecalculator.App
import com.d_ershov.mortgagecalculator.R

abstract class BaseSingleValidator() {
    var errorMsg = ""
    protected val resources = App.resourcesApp

    init {
        errorMsg = resources.getString(R.string.error_default)
    }
    abstract fun isValid(string: String) : Boolean
}