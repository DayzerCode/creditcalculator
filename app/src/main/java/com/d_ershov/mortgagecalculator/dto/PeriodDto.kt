package com.d_ershov.mortgagecalculator.dto

data class PeriodDto(
    var months: Int = 0,
    var years: Int = 0
) {
}