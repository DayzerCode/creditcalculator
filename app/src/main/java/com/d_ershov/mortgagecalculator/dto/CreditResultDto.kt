package com.d_ershov.mortgagecalculator.dto

data class CreditResultDto (val monthlyPayment : Double, val overPayment : Double, val totalSum : Double)