package com.d_ershov.mortgagecalculator

import android.app.Application
import android.content.res.Resources

class App : Application() {
    companion object {
        lateinit var resourcesApp: Resources
    }
    override fun onCreate() {
        super.onCreate()
        resourcesApp = this.resources
    }
}