package com.d_ershov.mortgagecalculator.calculators

import com.d_ershov.mortgagecalculator.dto.CreditResultDto
import kotlin.math.pow

class AnnuityCreditCalculator(
    private val sum: Int,
    private val percent: Double,
    private val countMonths: Int
) {
    fun getCreditResult(): CreditResultDto {
        val monthlyPercent = (percent / 12) / 100
        val powMonthlyPercent = (1 + monthlyPercent).pow(countMonths)
        val annuityCoefficient = (monthlyPercent * powMonthlyPercent) / (powMonthlyPercent - 1)

        val monthlyPayment = sum * annuityCoefficient
        val totalPayment = monthlyPayment * countMonths
        val overPayment = totalPayment - sum

        return CreditResultDto(monthlyPayment, overPayment, totalPayment)
    }
}