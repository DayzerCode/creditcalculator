package com.d_ershov.mortgagecalculator

import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.replaceText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.d_ershov.mortgagecalculator.constants.Calculator
import com.d_ershov.mortgagecalculator.ui.home.HomeFragment
import org.hamcrest.CoreMatchers.containsString
import org.hamcrest.CoreMatchers.not
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.annotation.LooperMode

@RunWith(AndroidJUnit4::class)
@LooperMode(LooperMode.Mode.PAUSED)
class HomeFragmentTest {

    @Before
    fun setUp() {
        launchFragmentInContainer<HomeFragment>()
    }

    @Test
    fun validationEtCreditSum() {
        // max sum
        onView(withId(R.id.etCreditSum)).perform(replaceText("100 000 500"))
        onView(withId(R.id.etCreditSum)).check(matches(hasErrorText(not(""))))
        onView(withId(R.id.etCreditSum)).check(matches(withText(not("100 000 500"))))
    }

    @Test
    fun validationEtPercentCredit() {
        // first symbols ,
        onView(withId(R.id.etPercentCredit)).perform(replaceText(","))
        onView(withId(R.id.etPercentCredit)).check(matches(hasErrorText(not(""))))
        onView(withId(R.id.etPercentCredit)).check(matches(withText(not(","))))
        // first symbols .
        onView(withId(R.id.etPercentCredit)).perform(replaceText("."))
        onView(withId(R.id.etPercentCredit)).check(matches(hasErrorText(not(""))))
        onView(withId(R.id.etPercentCredit)).check(matches(withText(not("."))))
        // insert 20,.,,.,.,.1
        onView(withId(R.id.etPercentCredit)).perform(replaceText("20,.,,.,.,.1"))
        onView(withId(R.id.etPercentCredit)).check(matches(hasErrorText(not(""))))
        onView(withId(R.id.etPercentCredit)).check(matches(withText(not(",.,,.,.,."))))
        // value > max_percent
        val exceededMaxPercent =  (Calculator.MAX_PERCENT + 1).toString()
        onView(withId(R.id.etPercentCredit)).perform(replaceText(exceededMaxPercent))
        onView(withId(R.id.etPercentCredit)).check(matches(withText(not(containsString(exceededMaxPercent)))))
    }
}
