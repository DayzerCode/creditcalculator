package com.d_ershov.mortgagecalculator.ui.listeners.validators

import org.junit.Assert.assertEquals
import org.junit.Test

class ValidatorDoubleTest {
    private val validator = ValidatorDouble()
    @Test
    fun `is valid value`() {
        val value = "25.10"
        assertEquals(true, validator.isValid(value))
    }
}
