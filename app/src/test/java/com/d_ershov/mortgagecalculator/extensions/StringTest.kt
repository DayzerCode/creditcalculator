package com.d_ershov.mortgagecalculator.extensions

import com.d_ershov.mortgagecalculator.extinsions.countChars
import com.d_ershov.mortgagecalculator.extinsions.replaceSpaces
import com.d_ershov.mortgagecalculator.extinsions.toFormat
import org.junit.Assert.assertEquals
import org.junit.Test

class StringTest {
    @Test
    fun `toFormat 10000 to 10 000`() {
        assertEquals("10 000", 10000.toFormat())
    }

    @Test
    fun `toFormat empty string`() {
        assertEquals("", "".toFormat())
    }

    @Test
    fun replaceSpacesTest() {
        assertEquals("abc", " a b c ".replaceSpaces())
    }

    @Test
    fun countCharsTest() {
        assertEquals(3, " a, b, c, d".countChars(","))
    }
}
