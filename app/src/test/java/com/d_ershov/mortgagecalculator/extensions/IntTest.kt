package com.d_ershov.mortgagecalculator.extensions

import com.d_ershov.mortgagecalculator.extinsions.declineWord
import com.d_ershov.mortgagecalculator.extinsions.roundUp
import com.d_ershov.mortgagecalculator.extinsions.splitMonthsByPeriod
import com.d_ershov.mortgagecalculator.extinsions.toFormat
import org.junit.Assert.assertEquals
import org.junit.Test

class IntTest {
    @Test
    fun `roundUp 1 000 333 to 1 000 000`() {
        assertEquals(1000000, 1000333.roundUp())
    }

    @Test
    fun `toFormat 1000 to 1 000`() {
        assertEquals("1 000", 1000.toFormat())
    }

    @Test
    fun `toFormat 0`() {
        assertEquals("0", 0.toFormat())
    }

    @Test
    fun splitMonthsByPeriodTest() {
        val period = 22.splitMonthsByPeriod()
        assertEquals(1, period.years)
        assertEquals(10, period.months)
    }

    @Test
    fun declineWordTest() {
        val declines = arrayOf("год", "года", "лет")
        assertEquals("1 год", 1.declineWord(declines))
        assertEquals("2 года", 2.declineWord(declines))
        assertEquals("5 лет", 5.declineWord(declines))
    }
}
